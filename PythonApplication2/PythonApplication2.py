'''
input's value will concider as follow
    0.0 <= x < 0.9    ==>   False
    0.9 <= x < 2.0    ==>   True

    defualt values shown by a comment in front of them


'''

#########################
class dendrite1:
    
    value       = float(0.0)  #defualt value
    weightVal   = float(10.0) #defualt Value
    
    def __init__(self, inputValue):
        self.value = float(inputValue)

    def weight(self):
        self.value = float(self.value) * float(self.weightVal)

def nucleus1(inputVal):

    if ( 9.0 <= inputVal and inputVal < 20.0 ):
        return 1
    elif ( 0.0 <= inputVal and inputVal < 9.0 ):
        return 0


class dendrite2:

    value     = float(0.0) #defualt Value
    weightVal = float(1.0) #defualt Value

    def __init__(self, inputVal):
        self.value = inputVal

    def weight(self):
        self.value = float(self.weightVal) * float(self.value)

def nucleus2(inputList):

    total = float(0.0) #defualt Value
    lengh = len(inputList) - 1
    
    for i in inputList:
        total = float(total) + i
    print(inputList)
    print(total)
    if (total == 1):
        print("XOR = 1")
    elif (total != 1):
        print("XOR = 0")
    
def synapse(value1, value2):
    val1 = dendrite2(value1)
    val1.weight()

    val2 = dendrite2(value2)
    val2.weight()

    myList = [val1.value , val2.value]
    nucleus2(myList)


x = dendrite1(0.9)
y = dendrite1(0.0)
x.weight()
y.weight()

synapse(nucleus1(x.value) , nucleus1(y.value))


a = dendrite1(1.9)
b = dendrite1(0.0)
a.weight()
b.weight()

synapse(nucleus1(a.value) , nucleus1(b.value))



c = dendrite1(1.9)
d = dendrite1(1.0)
c.weight()
d.weight()

synapse(nucleus1(c.value) , nucleus1(d.value))



e = dendrite1(0.9)
f = dendrite1(1.9)
e.weight()
f.weight()

synapse(nucleus1(e.value) , nucleus1(f.value))
